<?php namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\Notas;

class NotasTest extends TestCase
{
    protected $notas = null;

    public function setUp():void
    {
        // Como ainda nao foi usada, deve ser nula
        $this->assertNull($this->notas);
        $this->notas = new Notas;
    }
    
    public function testDeveCriarUmaInstanciaDeNotas()
    {
        // Pra ver se a instancia criada é a mesma que
        // esta na propriedade $notas.
        $expected = Notas::class;
        $this->assertInstanceOf($expected, $this->notas);
    }
 
    public function testDeveRetornarUmaNotaDe100()
    {
        // Uma nota de 100
        $expected = [100 => 1];
        $this->notas->setValor(100);
        $actual = $this->notas->calcularNotas();
        $this->assertEquals($expected, $actual);
    }

    public function testDeveRetornarDuasNotasDe100()
    {
        // Uma nota de 100
        $expected = [100 => 2];
        $this->notas->setValor(200);
        $actual = $this->notas->calcularNotas();
        $this->assertEquals($expected, $actual);
    }

    public function testDeveRetornarUmaNotaDe100EUmaDe50()
    {
        // Uma nota de 100
        $expected = [100 => 1, 50 => 1];
        $this->notas->setValor(150);
        $actual = $this->notas->calcularNotas();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Vamos supor que o usuario coloque um valor que nao seja um numero.
     * Se por acaso a entrada do usuario nao for um inteiro?
     *  - Retorna NULL.
     */
    public function testDeveRetornarNullSeInformadoUmaLetra()
    {
        $this->notas->setValor('a');
        $actual = $this->notas->getValor();
        $this->assertNull($actual);
    }

    /**
     * Se o usuario informar um  valor negativo?
     *  - Retorna zero.
     *
     * @return void
     */
    public function testDeveRetornarZeroSeInformadoUmValorNegativo()
    {
        $expected = 0;
        $this->notas->setValor(0);
        $actual =$this->notas->getValor();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Só por curiosidade, qual seria o valor para
     * que desse uma nota de cada?
     *
     * @return void
     */
    public function testDeveRetornarUmaNotaDeCada()
    {
        // Uma nota de 100
        $expected = [
            100 => 1,
            50 => 1,
            20 => 1,
            10 => 1,
            5 => 1,
            2 => 1,
            1 => 1
        ];
        $this->notas->setValor(188);
        $actual = $this->notas->calcularNotas();
        $this->assertEquals($expected, $actual);
    }
}

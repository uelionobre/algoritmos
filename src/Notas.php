<?php namespace Src;

/**
 * Classe de notas.
 */

class Notas
{
    private $cedulas = [100, 50, 20, 10, 5, 2, 1];
    private $valor = null;
    private $notasCalculadas = [];

    /**
     * Seta um valor para poder ser utilizada para
     * calcular o valor de cada nota.
     *
     * A validacao do tipo de dado e o valor informado
     * decidi deixar aqui para que o exemplo ficasse completo.
     *
     * @param integer $valor
     * @return void
     */
    public function setValor($valor = 0)
    {
        // Se nao for um inteiro.
        if (!is_integer($valor)) {
            return null;
        }

        // Se o valor nao for menor que zero.
        if ($valor < 0) {
            return 0;
        }
        
        $this->valor = (int) $valor;
    }

    /**
     * Retorna o valor informado ou nulo.
     *
     * @return int
     */
    public function getValor():?int
    {
        return $this->valor;
    }

    /**
     * Calcula as notas e armazena os resultados em $notasCalculadas
     *
     * @return array
     */
    public function calcularNotas() : array
    {
        // Percorrendo o array que contem minhas cedulas.
        // Neste caso, chamei as cedulas de notas.
        foreach ($this->cedulas as $cedula) {
            $quantidade = intval($this->getValor()/$cedula);
            $resto = $this->valor - ($quantidade*$cedula);

            if ($quantidade <= 0) {
                continue;
            }

            $this->notasCalculadas[$cedula] = $quantidade;
            $this->setValor($resto);
        }

        return $this->notasCalculadas;
    }
}

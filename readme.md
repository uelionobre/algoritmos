### Cálculo de notas de dinheiro ###

Um problema bem antigo das aulas de lógica de programação. Mas, eu não tinha visto.

Ele consiste em receber um valor e decompor em cedulas de 100, 50, 20, 10, 5, 2 e 1.

O nome do arquivo é `Notas.php`.

Fiz alguns testes simples, para executa-los rode o seguinte comando no seu terminal: `composer test`.

O resultado deve ser algo parecido com o mostrado abaixo:

```
PHPUnit 9.0.1 by Sebastian Bergmann and contributors.

Notas (Tests\Notas)
- ✔ Deve criar uma instancia de notas
- ✔ Deve retornar uma nota de 100
- ✔ Deve retornar duas notas de 100
- ✔ Deve retornar uma nota de 100 e uma de 50
- ✔ Deve retornar null se informado uma letra
- ✔ Deve retornar zero se informado um valor negativo
- ✔ Deve retornar uma nota de cada

Time: 33 ms, Memory: 4.00 MB

OK (7 tests, 14 assertions)
```

## Pacotes de terceiros utilizado ##
- PHPUnit


